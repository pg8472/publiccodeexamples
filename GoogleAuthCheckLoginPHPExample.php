<?php

  $token = filter_input(INPUT_COOKIE, 'remember_me', FILTER_SANITIZE_STRING);

  $userId = 0;

  $loggedIn = IsUserLoggedIn($link, $token);

  if($loggedIn){
    // echo "Logged In";
  }
  else{
    // echo "Not Logged In";
    header("Location: login");
  }

  function IsUserLoggedIn($link, $token){

    if (isset($_SESSION['userId'])) {
      return true;
    }

    if ($token && IsTokenValid($link, $token)){
      // echo "Token Exists";
      SetSessions($link, $GLOBALS["userId"]);
      header("Refresh:0");
    }
    else{
      return false;
    }
  }

  function IsTokenValid($link, $token){

    [$selector, $validator] = ParseToken($token);
    $tokens = FindUserTokenBySelector($link, $selector);

    if (!$tokens) {
        return false;
    }

    return password_verify($validator, $tokens);
  }

  function ParseToken($token){
      $parts = explode(':', $token);

      if ($parts && count($parts) == 2) {
          return [$parts[0], $parts[1]];
      }
      return null;
  }

  function FindUserTokenBySelector($link, $selector){

    $queryLocation = $link->prepare("SELECT `id`,`selector`,`hashed_validator`,`user_id`,`expiry`
                                    FROM `user_tokens`
                                    WHERE `selector`=? AND `expiry` >= now() LIMIT 1");

    $queryLocation->bind_param("s", $selector);
    $queryLocation->execute();
  
    $resultLocation = $queryLocation->get_result();
    $rowLocation = $resultLocation->fetch_assoc();

    $numRows = $resultLocation->num_rows;

    if($numRows > 0){
      $GLOBALS["userId"] = $rowLocation["user_id"];

      return $rowLocation["hashed_validator"];
    }
    else{
      return false;
    }
  }

  function SetSessions($link, $userId){

    $queryLocation = $link->prepare("SELECT `email`,`first_name`,`last_name`,`picture` FROM `users` WHERE `id`=?");

    $queryLocation->bind_param("i", $userId);
    $queryLocation->execute();
  
    $resultLocation = $queryLocation->get_result();
    $rowLocation = $resultLocation->fetch_assoc();

    $_SESSION["userId"] = $userId;
    $_SESSION["email"] = $rowLocation["email"];
    $_SESSION["firstName"] = $rowLocation["first_name"];
    $_SESSION["lastName"] = $rowLocation["last_name"];
    $_SESSION["picture"] = $rowLocation["picture"];
  }
?>