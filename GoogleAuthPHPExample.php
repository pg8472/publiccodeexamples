<?php

include "connection/connection.php";

session_start();

if(isset($_SESSION["user_id"])){
  header("Location: index");
}

require_once 'vendor/autoload.php';
  
GoogleAuth($link);

function GoogleAuth($link){
  $clientID = 'GOOGLE_CLIENT_ID_HERE';
  $clientSecret = 'GOOGLE_CLIENT_SECRET_HERE';
  $redirectUri = 'http://localhost/index';
    
  // create Client Request to access Google API
  $client = new Google_Client();
  $client->setClientId($clientID);
  $client->setClientSecret($clientSecret);
  $client->setRedirectUri($redirectUri);
  $client->addScope("email");
  $client->addScope("profile");
    
  // authenticate code from Google OAuth Flow
  if (isset($_GET['code'])) {
    $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
    $client->setAccessToken($token['access_token']);
    
    // get profile info
    $google_oauth = new Google_Service_Oauth2($client);
    $google_account_info = $google_oauth->userinfo->get();

    $email =  $google_account_info->email;
    $first_name =  $google_account_info->given_name;
    $last_name =  $google_account_info->family_name;
    $picture =  $google_account_info->picture;

    if(CheckIfUserExists($link, $email)){
      $userId = GetUserId($link, $email);
      DeleteTokens($link, $userId);
      [$token, $expired_seconds] = InsertUserToken($link, $userId);
      CreateSessions($userId, $email, $first_name, $last_name, $picture);
      CreateCookies($token, $expiry);
    }
    else{
      $userId = InsertNewUser($link, $email, $first_name, $last_name, $picture);
      DeleteTokens($link, $userId);
      [$token, $expired_seconds] = InsertUserToken($link, $userId);
      CreateSessions($userId, $email, $first_name, $last_name, $picture);
      CreateCookies($token, $expiry);
    }

    header("Location: index");
  } 
  else {
    echo "<a href='".$client->createAuthUrl()."'>Google Login</a>";
  }
}

function CheckIfUserExists($link, $email){
  $queryLocation = $link->prepare("SELECT * FROM `users` WHERE `email`=?");

  $queryLocation->bind_param("s", $email);
  $queryLocation->execute();
  $resultLocation = $queryLocation->get_result();
  
  $numRows = $resultLocation->num_rows;

  if($numRows == 0){
    return false;
  }
  else{
    return true;
  }
}

function InsertNewUser($link, $email, $first_name, $last_name, $picture){
  $queryLocation = $link->prepare("INSERT INTO `users` (`email`,`first_name`,`last_name`,`picture`) VALUES (?,?,?,?)");

  $queryLocation->bind_param("ssss", $email, $first_name, $last_name, $picture);
  $queryLocation->execute();

  return $link->insert_id;
}

function GetUserId($link, $email){
  $queryLocation = $link->prepare("SELECT `id` FROM `users` WHERE `email`=?");

  $queryLocation->bind_param("s", $email);
  $queryLocation->execute();

  $resultLocation = $queryLocation->get_result();
  $rowLocation = $resultLocation->fetch_assoc();

  return $rowLocation["id"];
}

function DeleteTokens($link, $userId){
  $queryLocation = $link->prepare("DELETE FROM `user_tokens` WHERE `user_id`=?");

  $queryLocation->bind_param("i", $userId);
  $queryLocation->execute();
}

function InsertUserToken($link, $userId){
  [$selector, $validator, $token] = GenerateTokens();

  $expired_seconds = time() + 60 * 60 * 24 * 30; // 30 Days
  $expiry = date('Y-m-d H:i:s', $expired_seconds);

  $query = $link->prepare("INSERT INTO `user_tokens` (`selector`, `hashed_validator`, `user_id`, `expiry`) VALUES (?,?,?,?)");
  $query->bind_param('ssis', $selector, $validator, $userId, $expiry);

  $query->execute();

  $query->close();
  $link->close();

  return [$token, $expired_seconds];
}

function GenerateTokens(){
  $selector = bin2hex(random_bytes(16));
  $validator = bin2hex(random_bytes(32));
  $token = $selector . ':' . $validator;

  return [$selector, $validator, $token];
}

function CreateSessions($userId, $email, $first_name, $last_name, $picture){
  $_SESSION["userId"] = $userId;
  $_SESSION["email"] = $email;
  $_SESSION["firstName"] = $first_name;
  $_SESSION["lastName"] = $last_name;
  $_SESSION["picture"] = $picture;
}

function CreateCookies($token, $expired_seconds){
  setcookie('remember_me', $token, $expired_seconds);
}

function ParseToken($token)
{
    $parts = explode(':', $token);

    if ($parts && count($parts) == 2) {
        return [$parts[0], $parts[1]];
    }
    return null;
}

?>