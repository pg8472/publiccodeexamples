<?php
session_start();

include "adminloggedincheck.php";
?>

<!DOCTYPE html>
<html>

<head>

<title>THOON Admin - Home</title>

<?php include "../globalcss.php"; ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../css/globalstyles.css">
<link rel="stylesheet" type="text/css" href="admincss/adminmain.css?v2.2">
<link rel="stylesheet" type="text/css" href="admincss/adminchangeproblem.css?v2.2">

</head>

<body style="background-color: #e6ffff;">

<?php
  $filename = basename(__FILE__);
  include "adminnavbar.php";
?>

<h1 class="centertext adminblue">
  <u>Admin - Home</u><br> <?php echo $adminname; ?>
</h1>

<?php

class CreateAdminMain{
  public $admin_main = "";

  function inner_container(){
    $text = '<div class="innercontainer">';
    $this->admin_main .= $text;
  }

  function header($header){
    $text = '<p><span class="headingtext"><strong><u>'.$header.'</u></strong></span></p>';
    $this->admin_main .= $text;
  }

  function close_div(){
    $text = '</div>';
    $this->admin_main .= $text;
  }

  function create_button($title, $href){
    $text = '<div class="buttonlinks buttonclick bluebackground" data-href="'.$href.'">
              <p class="buttontext"><strong>'.$title.'</strong></p>
            </div>';
    $this->admin_main .= $text;
  }

  function render(){
    echo $this->admin_main;
  }
}

$cam = new CreateAdminMain;

echo '<div class="outercontainer">';

$cam->inner_container();
$cam->header("Misc");
$cam->create_button("Yearly Settings", "admin_yearly_settings");
$cam->create_button("Yearly Calendar", "admin_calendar");
$cam->create_button("School List", "adminschools");
$cam->close_div();

$cam->inner_container();
$cam->header("Teams");
$cam->create_button("Teams By School", "adminteamsbyschool");
$cam->create_button("Teams Full List", "adminteamsall");
$cam->close_div();

$cam->inner_container();
$cam->header("Booklets"); 
$cam->create_button("Booklets", "adminbookletcheck");
$cam->create_button("Booklets PDF Print", "adminprintfriendly");
$cam->create_button("UP Examples", "adminupview");
$cam->close_div();

////////////////////////////////////////////////
$cam->inner_container();
$cam->header("Qualifying Problem (QP)");
$cam->create_button("QP Login & Release", "admin_yearly_settings");
$cam->create_button("QP Declaration", "admin_yearly_settings");
$cam->close_div();

$cam->inner_container();
$cam->header("QP Second Round");
$cam->create_button("1. Second Round Rankings", "admin_yearly_settings");
$cam->create_button("2. Second Round Allocation", "admin_yearly_settings");
$cam->create_button("3. Second Round Final List/Download", "admin_yearly_settings");
$cam->close_div();

$cam->inner_container();
$cam->header("Evaluator Allocation");
$cam->create_button("Booklet Stats", "admin_yearly_settings");
$cam->create_button("1. Pick Participating Evaluators", "admin_yearly_settings");
$cam->create_button("2. Pick Number Of Booklets", "admin_yearly_settings");
$cam->create_button("3. Randomise Booklets", "admin_yearly_settings");
$cam->create_button("4. Allocate Booklets To Evaluators", "admin_yearly_settings");
$cam->close_div();

$cam->inner_container();
$cam->header("Evaluator Information");
$cam->create_button("Evaluator Login & List", "admin_yearly_settings");
$cam->create_button("Scoresheets By Evaluators", "admin_yearly_settings");
$cam->create_button("Scoresheets By Schools", "admin_yearly_settings");
$cam->create_button("P1&P2 Certificates", "admin_yearly_settings");
$cam->create_button("Evaluator Completion Status", "admin_yearly_settings");
$cam->close_div();

$cam->inner_container();
$cam->header("National Finals");
$cam->create_button("Students", "admin_yearly_settings");
$cam->create_button("National Finals Register", "admin_yearly_settings");
$cam->create_button("National Finals Collated Stats", "admin_yearly_settings");
$cam->close_div();

$cam->render();

echo '</div>';

?>

<p>&copy <?php echo date("Y"); ?> Future Problem Solving New Zealand - All Rights Reserved</p>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="adminjs/adminmain2.js?v1.03"></script>
<script type="text/javascript" src="adminjs/adminchangeproblem.js?v1.02"></script>

</body>

</html>