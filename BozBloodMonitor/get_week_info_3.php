<?php

include "connection.php";

$text = '';

date_default_timezone_set('Pacific/Auckland');

$monthPost = $_POST["month"];
$year = "2022";

$month = new DateTime($year ."-" . $monthPost . "-01");

$todaysDay = date("d");
$todaysMonth = date("m");

$firstDayOfMonth = $month->format("N");
$daysInMonthCount = $month->format("t");

//echo $firstDayOfMonth;
$text .= TableHeader();

$text .= TableBody($firstDayOfMonth, $daysInMonthCount, $monthPost, $year, $todaysDay, $todaysMonth);

$text .= TableFooter();

function TableHeader(){
    $table = '';
    $table .= '<table class="table table-striped" id="generalTable" style="width:1200px;">';
    $table .= '<thead class="thead-dark">';
    $table .= '<tr>';
    $table .= '<th>Mon</th>';
    $table .= '<th>Tue</th>';
    $table .= '<th>Wed</th>';
	$table .= '<th>Thur</th>';
	$table .= '<th>Fri</th>';
	$table .= '<th>Sat</th>';
	$table .= '<th>Sun</th>';
    $table .= '</tr>';
    $table .= '</thead>';
    $table .= '<tbody>';

    return $table;
}

function TableBody($firstDayOfMonth, $daysInMonthCount, $monthPost, $year, $todaysDay, $todaysMonth){

	$table = '';
	$dayCount = 1;

	for($i = 0; $i < 6; $i++){

		$table .= '<tr>';

		for($j = 0; $j < 7; $j++){

			if($i == 0){
				if(($j+1) >= $firstDayOfMonth){

					if($todaysDay == $dayCount && $todaysMonth == $monthPost){
						$table .= '<td><div class="test">'.DailyInput($dayCount, $monthPost, $year).'</div></td>';
					}
					else{
						$table .= '<td>'.DailyInput($dayCount, $monthPost, $year).'</td>';
					}

					$dayCount++;
				}
				else{
					$table .= '<td></td>';
				}
			}
			else{
				if($dayCount <= $daysInMonthCount){

					if($todaysDay == $dayCount && $todaysMonth == $monthPost){
						$table .= '<td><div class="test">'.DailyInput($dayCount, $monthPost, $year).'</div></td>';
					}
					else{
						$table .= '<td>'.DailyInput($dayCount, $monthPost, $year).'</td>';
					}
					
					$dayCount++;
				}
				else{
					$table .= '<td></td>';
				}
			}
		}
		$table .= '</tr>';
	}

    return $table;
}

function TableFooter(){
    $table = '</tbody>';
    $table .= '</table>';

    return $table;
}

function DailyInput($dayCount, $monthPost, $year){
	return '<div class="dailyContainer">
	<label>'.ordinal_suffix_of($dayCount).'</label><br>

	<label>Morning: </label>&nbsp<input class="morningText" type="number" data-day="'.$dayCount.'" 
	data-month="'.$monthPost.'" data-year="'.$year.'" data-time-of-day="morning" 
	style="width: 50px;" value="'.GetValueIfExists($dayCount, $monthPost, $year, "morning").'" /><br>

	<label>Evening: </label>&nbsp<input class="morningText" type="number" data-day="'.$dayCount.'" 
	data-month="'.$monthPost.'" data-year="'.$year.'" data-time-of-day="evening" 
	style="width: 50px;" value="'.GetValueIfExists($dayCount, $monthPost, $year, "evening").'" /><br>

	</div>';
}

function GetValueIfExists($dayCount, $monthPost, $year, $timeOfDay){
	$link = $GLOBALS['linkGlobal'];
	$text = '';

	$dateToCheck = $year . "-" . $monthPost . "-" . $dayCount;
	$dateConverted = date('Y-m-d', strtotime($dateToCheck));

	$query = $link->prepare("SELECT * FROM `boz_bl_monitor` WHERE `date_recorded`=? AND `time_of_day`=?");
	$query->bind_param("ss", $dateConverted, $timeOfDay);

	$query->execute();

	$result = $query->get_result();
	$row = $result->fetch_assoc();

	if(isset($row["blood_level"])){
		return $row["blood_level"];
	}
}

function ordinal_suffix_of($i) {
	$j = $i % 10;
		$k = $i % 100;
	if ($j == 1 && $k != 11) {
		return $i . "st";
	}
	if ($j == 2 && $k != 12) {
		return $i . "nd";
	}
	if ($j == 3 && $k != 13) {
		return $i . "rd";
	}
	return $i . "th";
  }

echo $text;

?>