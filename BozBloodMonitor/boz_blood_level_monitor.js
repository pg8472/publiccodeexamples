$( document ).ready(function(){

    $("#monthLeft").on("click", function(){
        var currentMonth = $("#month").prop('selectedIndex');

        if(currentMonth > 0){
            $("#month").prop('selectedIndex', currentMonth-1).trigger('change');
        }
    });

    $("#monthRight").on("click", function(){
        var currentMonth = $("#month").prop('selectedIndex');

        if(currentMonth < 11){
            $("#month").prop('selectedIndex', currentMonth+1).trigger('change');
        }
    });

    $(document).on("input propertychange", ".morningText", function(){
        var day = $(this).data("day");
        var month = $(this).data("month");
        var year = $(this).data("year");
        var timeOfDay = $(this).data("time-of-day");
        var value = $(this).val();

        // console.log(day, month, year, timeOfDay, value);

        $.ajax({
            url: "connection/update_boz_bl_monitor.php",
            type: 'POST',
            data: {day, month, year, timeOfDay, value},
            success: function(data) {
                console.log(data);
            },
            error: function(xhr, status, error) {
                console.log(xhr.status, status, error);
            }
        });
    });

    var month = $("#month").val();
    LoadWeeks(month);

    $("#month").on("change", function(){
        var month = $(this).val();
        LoadWeeks(month);
    });

    function LoadWeeks(month){
        $.ajax({
            url: "connection/get_week_info_3.php",
            type: 'POST',
            data: {month},
            success: function(data) {
                // console.log(data);
                
                $("#weekly").html(data);
            },
            error: function(xhr, status, error) {
                console.log(xhr.status, status, error);
            }
        });
    }
});